#pragma once
#include <string>

class OwnClass
{
public:
	std::string_view GetSomeData() const;
	void SetSomeData(const std::string& data);
private:
	std::string someData;
};


//https://ru.onlinemschool.com/math/library/vector/length/
#include <cstdlib>
#include "Vector.h"
#include "OwnClass.h"

int main()
{
	std::cout << "Base vector 2 4 4"<<std::endl;
	Vector tmp(2,4,4);
	std::cout<<"Vector length = "<< tmp.Length()<<std::endl;
	OwnClass myClass;
	const std::string tmpData("123QWERTY");
	std::cout << "Own class set data " << tmpData << std::endl;
	myClass.SetSomeData(tmpData);
	std::cout << "Own class get data ";
	std::cout<<myClass.GetSomeData()<<std::endl;
	system("pause");
	return EXIT_SUCCESS;
}

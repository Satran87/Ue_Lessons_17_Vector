#include "Vector.h"

void Vector::Show()
{
	std::cout << x << " " << y << " " << z << std::endl;
}

double Vector::Length()
{
	return sqrt(x*x + y*y+z*z);
}

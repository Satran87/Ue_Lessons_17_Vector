#pragma once
#include <iostream>
class Vector
{
public:
	Vector() = default;
	Vector(double _x, double _y, double _z) :x(_x), y(_y), z(_z)
	{}
	void Show();
	double Length();
private:
	double x = 0;
	double y = 0;
	double z = 0;
};

